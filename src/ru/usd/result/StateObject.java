package ru.usd.result;

/**
 * Счетчик
 *
 */

public class StateObject {
    private int i;

    synchronized void increment(){
        i++;
    }

    public int getI(){
        return i;
    }
}